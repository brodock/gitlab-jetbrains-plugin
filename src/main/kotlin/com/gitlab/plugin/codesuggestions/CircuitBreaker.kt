package com.gitlab.plugin.codesuggestions

import io.ktor.util.date.*

class CircuitBreaker(private val maxErrorsBeforeBreaking: Int, private val breakTimeMs: Int) {
  private var errorCount = 0
  private var tryAgainAfterTimestamp: Long = 0

  fun error() {
    errorCount += 1
    if (errorCount >= maxErrorsBeforeBreaking) {
      tryAgainAfterTimestamp = getTimeMillis() + breakTimeMs
    }
  }

  fun isBreaking() : Boolean = (getTimeMillis() < tryAgainAfterTimestamp)

  fun success() {
    errorCount = 0
    tryAgainAfterTimestamp = 0
  }
}
