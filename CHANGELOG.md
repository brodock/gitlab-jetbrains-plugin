# Changelog

## [Unreleased]

### Added

### Changed

### Removed

### Fixed

## [0.1.1] - 2023-07-21

### Added
- Settings screen to configure a GitLab instance and credentials for the Plugin
- Status bar icon to track configuration and busy/ready state
- GitLab Duo Code Suggestions initial integration

[Unreleased]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.1...HEAD
[0.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/commits/v0.1.1
