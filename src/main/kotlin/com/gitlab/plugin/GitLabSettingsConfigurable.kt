package com.gitlab.plugin

import com.gitlab.plugin.authentication.GitLabPersistentAccount
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.TokenUtil
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import java.util.*

class GitLabSettingsConfigurable : BoundConfigurable(GitLabBundle.message("settings.ui.group.name")) {

  private var tokenText = TokenUtil.getToken() ?: ""
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private var settingsPanel: DialogPanel? = null

  override fun createPanel(): DialogPanel {
    val account = GitLabPersistentAccount.getInstance()

    settingsPanel = panel {
      row(bundle.getString("settings.ui.gitlab.url")) {
        textField().bindText(account.state::url)
          .addValidationRule(bundle.getString("settings.ui.error.url.invalid")) { it.text.isBlank() }
          .addValidationRule(bundle.getString("settings.ui.error.url.invalid")) { !GitLabUtil.isValidURL(it.text) }
      }
      row(bundle.getString("settings.ui.gitlab.token")) {
        passwordField()
          .bindText(::tokenText)
          .focused()
      }
    }

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()
      TokenUtil.setToken(tokenText)

      notifyChange(tokenText)
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }

  private fun notifyChange(token: String) {
    val account = GitLabPersistentAccount.getInstance()
    account.state.enabled = token.isNotEmpty()
    // TODO: Add notification publisher code here
  }
}
