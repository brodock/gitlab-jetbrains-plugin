package com.gitlab.plugin.api

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.lang.System.currentTimeMillis

@Serializable
data class CodeSuggestionsTokenDto(
  @SerialName("access_token") val accessToken: String,
  @SerialName("expires_in") val expiresIn: Long,
  @SerialName("created_at") val createdAt: Long
) {

  companion object {
    /**
     * Amount of time we remove from expected expiration time
     * to prevent false positives (we think it was not expired but
     * by the time the request happens it is expired)
     */
    private const val JWT_EXPIRATION_LEEWAY: Int = 60
  }

  /**
   * Return whether the token has expired or not
   */
  fun isExpired(): Boolean {
    val willExpireAfter = createdAt + expiresIn - JWT_EXPIRATION_LEEWAY
    val currentTime = currentTimeMillis()/1000

    return currentTime > willExpireAfter
  }
}
