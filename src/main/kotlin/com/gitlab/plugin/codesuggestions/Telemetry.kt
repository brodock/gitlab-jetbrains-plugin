package com.gitlab.plugin.codesuggestions

import com.intellij.openapi.diagnostic.Logger
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

data class Model(
  val engine: String = "",
  val name: String = "",
  val lang: String = ""
)

data class TelemetryPayload(
  val modelEngine: String,
  val modelName: String,
  val lang: String,
  val errors: Int
)

object Telemetry {
  private data class Counters(val errors: AtomicInteger = AtomicInteger(0))

  private val telemetryByModel = ConcurrentHashMap<Model, Counters>()
  private val logger = Logger.getInstance(Telemetry::class.java)

  fun incrementErrorCount(model: Model) {
    telemetryByModel
      .getOrPut(model) { Counters() }
      .errors
      .incrementAndGet()
  }

  fun getAndReset(): List<TelemetryPayload> {
    return telemetryByModel.map {
      TelemetryPayload(
        modelEngine = it.key.engine,
        modelName = it.key.name,
        lang = it.key.lang,
        errors = it.value.errors.getAndSet(0)
      )
    }
  }

  fun logAndReset(): List<TelemetryPayload> {
    val payload = getAndReset()

    logger.info("Telemetry payload (not being sent): $payload")

    return emptyList()
  }
}
