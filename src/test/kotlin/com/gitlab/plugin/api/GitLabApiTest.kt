package com.gitlab.plugin.api

import com.intellij.testFramework.fixtures.BasePlatformTestCase
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.util.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import java.net.URI

class GitLabApiTest : BasePlatformTestCase() {

  private val gitlabApi by lazy {
    GitLabApi("mycustomtoken", "https://gitlab.example.com")
  }

  @BeforeEach
  public override fun setUp() {
    super.setUp()
  }

  @Test
  fun `returns an ApiUrl based on gitLabHost`() {
    val expectedUri = URI("https://gitlab.example.com/api/v4/")

    assertThat(gitlabApi.apiUri).isEqualTo(expectedUri)
  }

  @Nested
  @DisplayName("with RestClient")
  inner class WithRestClient {
    private val attributes = gitlabApi.restClient.attributes[AttributeKey<Attributes>("ApplicationPluginRegistry")]

    @Test
    fun `ensure UserAgent is configured`() {
      // Assert UserAgent format
      val userAgent = attributes[UserAgent.key]
      Assertions.assertLinesMatch(
        listOf("gitlab-jetbrains-plugin/[0-9][.][0-9][.][0-9] IntelliJ-IDEA/.*"), listOf(userAgent.agent)
      )
    }

    @Test
    fun `ensure Auth provider is configured to used Bearer token`() {
      // Assert token authentication is configured
      val authProvider = attributes[Auth.key].providers[0]
      assertTrue(authProvider is BearerAuthProvider)
    }
  }
}

