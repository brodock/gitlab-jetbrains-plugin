package com.gitlab.plugin.ui

import com.gitlab.plugin.ui.NotificationAction

/**
 * A notification that can be displayed on screen
 *
 * See the [Balloon UI Guidelines](https://jetbrains.design/intellij/controls/balloon/) for more information
 * @param title will be displayed in bold in the first line
 * @param message the content of the notification
 * @param actions a list of clickable actions to include in the notification
 */
data class Notification(val title: String = "", val message: String, val actions: List<NotificationAction> = listOf()) {

}
