package com.gitlab.plugin.api

import com.gitlab.plugin.util.GitLabUtil
import com.intellij.collaboration.util.resolveRelative
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.net.URI

private const val DEFAULT_SERVER_URL = "https://gitlab.com"

class GitLabUnauthorizedException(response: HttpResponse, cachedResponseText: String) :
  ResponseException(response, cachedResponseText) {
  override val message: String =
    "Invalid authentication for Request: ${response.call.request.url}, " +
      "Method: ${response.call.request.method.value}, Status: ${response.status}."
}

class GitLabOfflineException(request: HttpRequest, cause: Throwable) : io.ktor.utils.io.errors.IOException() {
  override val message: String =
    "Could not complete Request: ${request.url}, " +
      "Method: ${request.method.value}, Error: ${cause::class.qualifiedName}."
}

class GitLabApi(private val token: String, gitlabHost: String = DEFAULT_SERVER_URL) {
  private val baseUrl: URI

  init {
    baseUrl = URI(gitlabHost)
  }

  val restClient = HttpClient(CIO) {
    expectSuccess = true

    install(UserAgent) {
      agent = GitLabUtil.userAgent
    }
    install(HttpRequestRetry) {
      retryOnServerErrors(maxRetries = 3)
      exponentialDelay()
    }
    install(Auth) {
      bearer {
        loadTokens {
          BearerTokens(token, "")
        }
      }
    }
    install(ContentNegotiation) {
      json(Json {
        isLenient = true
      })
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { exception, _ ->
        val clientException = exception as? ClientRequestException ?: return@handleResponseExceptionWithRequest
        val exceptionResponse = clientException.response

        if (exceptionResponse.status == HttpStatusCode.Unauthorized) {
          val exceptionResponseText = exceptionResponse.bodyAsText()
          throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
        }
      }

      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        when (cause::class) {
          java.nio.channels.UnresolvedAddressException::class -> {
            throw GitLabOfflineException(request, cause)
          }

          io.ktor.client.network.sockets.ConnectTimeoutException::class -> {
            throw GitLabOfflineException(request, cause)
          }
        }
      }
    }
  }

  val apiUri: URI
    get() = baseUrl.resolveRelative("api/v4/")

  /**
   * Return whether is correctly configured to perform requests
   */
  fun isConfigured(): Boolean {
    return token.isNotEmpty()
  }

  /**
   * Generate code suggestions token
   *
   * @return CodeSuggestionsTokenDto - an object containing the access token and expiration metadata
   */
  @Throws(GitLabUnauthorizedException::class, GitLabOfflineException::class)
  suspend fun generateCodeSuggestionsToken(): CodeSuggestionsTokenDto? {
    val uri = apiUri.resolveRelative("code_suggestions/tokens").toString()

    val response: HttpResponse = restClient.post(uri) {
      contentType(ContentType.Application.Json)
    }

    return response.body<CodeSuggestionsTokenDto>()
  }
}
