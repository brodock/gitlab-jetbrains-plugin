package com.gitlab.plugin.codesuggestions

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Nested

class TelemetryTest {
  private val model1 = Model(engine = "test-engine", name = "test-name", lang = "test-lang")
  private val model2 = Model(engine = "test-engine-2", name = "test-name-2", lang = "test-lang-2")

  @BeforeEach
  fun setUp() {
    Telemetry.getAndReset()
  }

  @Test
  fun `incrementErrorCount increments the number of errors for the given model`() {
    Telemetry.incrementErrorCount(model1)
    Telemetry.incrementErrorCount(model1)
    Telemetry.incrementErrorCount(model2)
    Telemetry.incrementErrorCount(model2)
    Telemetry.incrementErrorCount(model2)

    val payloadList = Telemetry.getAndReset()

    payloadList
      .filter { it.modelName == model1.name }
      .let {
        assertThat(it).hasSize(1)

        val payload = it.first()
        assertThat(payload.modelEngine).isEqualTo(model1.engine)
        assertThat(payload.lang).isEqualTo(model1.lang)
        assertThat(payload.errors).isEqualTo(2)
      }

    payloadList
      .filter { it.modelName == model2.name }
      .let {
        assertThat(it).hasSize(1)

        val payload = it.first()
        assertThat(payload.modelEngine).isEqualTo(model2.engine)
        assertThat(payload.lang).isEqualTo(model2.lang)
        assertThat(payload.errors).isEqualTo(3)
      }
  }

  @Suppress("ClassName")
  @Nested
  inner class getAndReset {
    @Test
    fun `resets all counters for all models`() {
      Telemetry.incrementErrorCount(model1)
      Telemetry.incrementErrorCount(model2)

      Telemetry.getAndReset()

      assertThat(Telemetry.getAndReset()).allSatisfy {
        assertThat(it.errors).isZero()
      }
    }

    @Test
    fun `returns a TelemetryPayload for every unique model`() {
      Telemetry.incrementErrorCount(model1.copy(name = "test-name-3"))
      Telemetry.incrementErrorCount(model1.copy(engine = "test-engine-3"))
      Telemetry.incrementErrorCount(model1.copy(lang = "test-lang-3"))

      val payloadList = Telemetry.getAndReset()

      assertThat(payloadList.filter { it.modelName == "test-name-3" }).hasSize(1)
      assertThat(payloadList.filter { it.modelEngine == "test-engine-3" }).hasSize(1)
      assertThat(payloadList.filter { it.lang == "test-lang-3" }).hasSize(1)
    }
  }

  @Suppress("ClassName")
  @Nested
  inner class logAndReset {
    @Test
    fun `resets all counters for all models`() {
      Telemetry.incrementErrorCount(model1)
      Telemetry.incrementErrorCount(model2)

      Telemetry.logAndReset()

      assertThat(Telemetry.getAndReset()).allSatisfy {
        assertThat(it.errors).isZero()
      }
    }

    @Test
    fun `returns an empty list`() {
      Telemetry.incrementErrorCount(model1)
      Telemetry.incrementErrorCount(model2)

      assertThat(Telemetry.logAndReset()).isEmpty()
    }
  }
}
